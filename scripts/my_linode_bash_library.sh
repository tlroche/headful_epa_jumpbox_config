#!/bin/bash

### Just some functions to add to or replace those in the StackScript Bash Library

### https://www.linode.com/stackscripts/view/1 [note: HTML, not raw!]

### so remember to source that first, if desired. These functions depend on

### * modern `bash` with `local`
### * aptitude
### * chmod
### * chown
### * cp
### * fgrep
### * mkdir
### * mktemp
### * sed

### ----------------------------------------------------------------------
### functions
### ----------------------------------------------------------------------

### Copy/mod of function=goodstuff from StackScript Bash Library (which this overrides)
### Depends on global definition of envvars={JUMPBOX_MINIMAL_BASE_PACKAGE_LIST, JUMPBOX_BASE_PACKAGE_LIST}
function goodstuff {
  if [[ -z "${JUMPBOX_BASE_PACKAGE_LIST}" ]] ; then
    aptitude -y install ${JUMPBOX_MINIMAL_BASE_PACKAGE_LIST}
  else
    aptitude -y install ${JUMPBOX_BASE_PACKAGE_LIST}
  fi
  ## tweaks for root
  sed -i -e 's/^#PS1=/PS1=/' /root/.bashrc # enable the colorful root bash prompt
  sed -i -e "s/^#alias ll='ls -l'/alias ll='ls -al'/" /root/.bashrc # enable ll list long alias
}

### Copy/mod of function=user_add_pubkey from StackScript Bash Library (which this overrides).
### Adds the user's public key to authorized_keys for the specified user. (This adds `chmod` of SSH dir and keys file.)
### Quote inputs, or the key may not load properly.
function user_add_pubkey {
  # $1 - Required - username
  # $2 - Required - public key
  local USERNAME="${1}"
  local USERPUBKEY="${2}"
  local USERSSHDIR=''
  local MESSAGE_PREFIX='my_linode_bash_library.sh::user_add_pubkey:'
  local WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${USERNAME}" || -z "${USERPUBKEY}" ]] ; then
    echo -e "${ERROR_PREFIX} must provide a username and a public SSH key" 1>&2 # stderr
    exit 2
  elif [[ "${USERNAME}" == 'root' ]] ; then
    USERSSHDIR='/root/.ssh'
  else
    USERSSHDIR="/home/${USERNAME}/.ssh"
  fi
  local USERKEYSFILE="${USERSSHDIR}/authorized_keys"

  mkdir -p "${USERSSHDIR}/"
  echo -e "${USERPUBKEY}" >> "${USERKEYSFILE}"
  if [[ "${USERNAME}" != 'root' ]] ; then
    chown -R "${USERNAME}":"${USERNAME}" "${USERSSHDIR}"
    chmod 0700 "${USERSSHDIR}"
    chmod 0600 "${USERKEYSFILE}"
  fi
}

### Quick'n'dirty "backup" of a file: just copy FN->FN.0 and make latter read-only.
### TODO: test if current user lacks sufficient privileges for file to backup.
### TODO: refactor: almost identical to function=sudo_backup below.
function backup {
  local BACKUP_FP="${1}"  # file to backup
  local BACKED_UP_FP="${BACKUP_FP}.0"
  local MESSAGE_PREFIX='my_linode_bash_library.sh::backup:'
  local WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${BACKUP_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} must provide path to file to backup" 1>&2 # stderr
#    return 1; # use `return` for StackScript Bash Library
    exit 3
  elif [[ ! -r "${BACKUP_FP}" ]] ; then
    echo -e "${WARNING_PREFIX} nop: cannot read file to backup='${BACKUP_FP}'"
    return 7 # not `exit`! warning, not error
  elif [[ -r "${BACKED_UP_FP}" ]] ; then
    echo -e "${WARNING_PREFIX} nop: previous backup='${BACKED_UP_FP}' exists"
    return 8 # not `exit`! warning, not error
  else
    cp ${BACKUP_FP} ${BACKED_UP_FP}
    chmod a-w ${BACKED_UP_FP}
    ls -al ${BACKUP_FP} ${BACKED_UP_FP}
  fi
}

### Quick'n'dirty "backup" of a non-user-owned file.
### TODO: refactor: almost identical to function=backup above.
function sudo_backup {
  local BACKUP_FP="${1}"  # file to backup
  local BACKED_UP_FP="${BACKUP_FP}.0"
  local MESSAGE_PREFIX='my_linode_bash_library.sh::backup:'
  local WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${BACKUP_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} must provide path to file to backup" 1>&2 # stderr
#    return 1; # use `return` for StackScript Bash Library
    exit 3
  elif [[ ! -r "${BACKUP_FP}" ]] ; then
    echo -e "${WARNING_PREFIX} nop: cannot read file to backup='${BACKUP_FP}'"
    return 7 # not `exit`! warning, not error
  elif [[ -r "${BACKED_UP_FP}" ]] ; then
    echo -e "${WARNING_PREFIX} nop: previous backup='${BACKED_UP_FP}' exists"
    return 8 # not `exit`! warning, not error
  else
    sudo cp ${BACKUP_FP} ${BACKED_UP_FP}
    sudo chmod a-w ${BACKED_UP_FP}
    ls -al ${BACKUP_FP} ${BACKED_UP_FP}
  fi
}

### Ensure that $KEY has $VAL in space-delimited .conf file=$CONF_FP with crude strategy:
### * comment out any line containing our key
### * append line containing our (key, value) pair
function ensure_conf_file_key_value {
  local CONF_FP="${1}"
  local KEY="${2}"
  local VAL="${3}"
  local TMP_FP=''  # temporary output file
  local LINE=''    # line(s) from input

  # for logging
  local MESSAGE_PREFIX='my_linode_bash_library.sh::ensure_conf_file_key_value:'
  local WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${CONF_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} must provide path to .conf file to test" 1>&2 # stderr
#    return 1; # use `return` for StackScript Bash Library
    exit 4
  elif [[ -z "${KEY}" ]] ; then
    echo -e "${ERROR_PREFIX} must provide key to lookup in .conf file" 1>&2
#    return 1; # use `return` for StackScript Bash Library
    exit 5
  elif [[ -z "${VAL}" ]] ; then
    echo -e "${ERROR_PREFIX} must provide value for key to lookup in .conf file" 1>&2
#    return 1; # use `return` for StackScript Bash Library
    exit 6
  elif [[ ! -r "${CONF_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read .conf file='${CONF_FP}'" 1>&2
    exit 7
  fi

  backup "${CONF_FP}" # function=backup above
  TMP_FP="$(mktemp)"
  ## Read each line from CONF_FP. If line contains KEY, comment line. Write line to TMP_FP.
  while read LINE ; do
    # TODO: better: `expr`?
    if [[ -n "$(echo -e "${LINE}" | fgrep -e "${KEY}")" ]] ; then
      LINE="# ${LINE}"
    fi
    echo -e "${LINE}" >> "${TMP_FP}"
  done < "${CONF_FP}"
  ## Write desired line
  echo -e "${KEY} ${VAL}" >> "${TMP_FP}"
  ## Write desired file
  mv "${TMP_FP}" "${CONF_FP}"
  if [[ "$?" == 0 ]] ; then
    ## Success! show result(s)
    echo -e "${MESSAGE_PREFIX} ls -alt ${CONF_FP}*:"
    ls -alt ${CONF_FP}*
  else
    echo -e "${ERROR_PREFIX} could not replace .conf file='${CONF_FP}' with tempfile='${TMP_FP}'" 1>&2 # stderr
    exit 8
  fi
} # function ensure_conf_file_key_value
