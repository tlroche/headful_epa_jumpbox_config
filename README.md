Goto [the wiki](headful_epa_jumpbox_config/wiki/Home). 

**TODO:** make FR: making the overview page point to `./wiki/Home` instead of `./README` should be a project-creation option.

(Note: that [`./wiki/Home`](./wiki/Home) currently fails (as of 27 Apr 2015), requiring instead a "Bitbucket-style," pseudo-relative link=`project_name/wiki/Home` (see first link above), is [already an issue](https://bitbucket.org/site/master/issue/6589/markdown-relative-link-to-image-and-other).)
