#!/usr/bin/env bash

### A fairly minimal, reasonably secure foundation for whatever you might be doing on a debian-based linode.
### Mostly this just automates

### 1. https://www.linode.com/docs/getting-started/
### 2. https://www.linode.com/docs/security/securing-your-server/

### However this does *not* remove account=root, since some of my subsequent installs seem to require that.
### (This *does* remove SSH login by account=root, however.)

### This script assumes you

### 1. have previously completed the pre-install process described @
###    https://bitbucket.org/tlroche/headful_epa_jumpbox_config/wiki/Home#rst-header-pre-install-process

### 2. have previously completed the pre-script process (not the pre-StackScript process) described @
###    https://bitbucket.org/tlroche/headful_epa_jumpbox_config/wiki/base_install#rst-header-pre-script-process

### 3. run this script as described @
###    https://bitbucket.org/tlroche/headful_epa_jumpbox_config/wiki/base_install#rst-header-script-base-install

### Note that linode.com reservation to itself (for StackScripts) of names for environment variables prefixed 'linode_' (see https://forum.linode.com/viewtopic.php?f=20&t=11419).

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

### just for logging

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
# for manual testing
#THIS_FN='minimal_secure_debian-based_linode_config.sh'
MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### user properties

## these properties files must be in THIS_DIR ... or change code below
## but the point of properties files is largely to avoid touching code
JUMPBOX_PRIVATE_PROPERTIES_FN='private.properties' # filename
JUMPBOX_PUBLIC_PROPERTIES_FN='public.properties'   # ditto

## private properties

JUMPBOX_PRIVATE_PROPERTIES_FP="${THIS_DIR}/${JUMPBOX_PRIVATE_PROPERTIES_FN}" # filepath
if   [[ -z "${JUMPBOX_PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_PRIVATE_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  exit 2
elif [[ ! -r "${JUMPBOX_PRIVATE_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read private properties file='${JUMPBOX_PRIVATE_PROPERTIES_FP}', exiting ..." 1>&2
  exit 3
else
  source "${JUMPBOX_PRIVATE_PROPERTIES_FP}"
  # TODO: test error/return code
fi

# remember to set JUMPBOX_USER_PASSWORD from commandline!
if   [[ -z "${JUMPBOX_USER_PASSWORD}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_USER_PASSWORD not defined, exiting ..." 1>&2 # stderr
  exit 4
# rest should be set in private.properties
elif [[ -z "${JUMPBOX_USER_NAME}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_USER_NAME not defined, exiting ..." 1>&2
  exit 5
elif [[ -z "${JUMPBOX_USER_PUBLIC_SSHKEY}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_USER_PUBLIC_SSHKEY not defined, exiting ..." 1>&2
  exit 6
elif [[ -z "${JUMPBOX_HOSTNAME}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_HOSTNAME not defined, exiting ..." 1>&2
  exit 7
elif [[ -z "${JUMPBOX_IPV4}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_IPV4 not defined, exiting ..." 1>&2
  exit 8
elif [[ -z "${JUMPBOX_IPV6}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_IPV6 not defined, exiting ..." 1>&2
  exit 9
elif [[ -z "${JUMPBOX_FQDN}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_FQDN not defined, exiting ..." 1>&2
  exit 10
elif [[ -z "${JUMPBOX_TZ_LOCATION}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_TZ_LOCATION not defined, exiting ..." 1>&2
  exit 11
fi

# derived from private properties

JUMPBOX_USER_SSH_DIR="${JUMPBOX_USER_HOME}/.ssh"
JUMPBOX_USER_KEYS_FP="${JUMPBOX_USER_SSH_DIR}/authorized_keys"

JUMPBOX_IPV4_IN_HOSTS_FILE="${JUMPBOX_IPV4} ${JUMPBOX_FQDN} ${JUMPBOX_HOSTNAME}"
JUMPBOX_IPV6_IN_HOSTS_FILE="${JUMPBOX_IPV6} ${JUMPBOX_FQDN} ${JUMPBOX_HOSTNAME}"

## public properties

JUMPBOX_PUBLIC_PROPERTIES_FP="${THIS_DIR}/${JUMPBOX_PUBLIC_PROPERTIES_FN}" # filepath
if   [[ -z "${JUMPBOX_PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  exit 12
elif [[ ! -r "${JUMPBOX_PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read public properties file='${JUMPBOX_PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  exit 13
else
  source "${JUMPBOX_PUBLIC_PROPERTIES_FP}"
  # TODO: test error/return code
fi

# these should all be set in public.properties
if   [[ -z "${JUMPBOX_BASE_PACKAGE_LIST}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_BASE_PACKAGE_LIST not defined, exiting ..." 1>&2
  exit 14
elif [[ -z "${JUMPBOX_FIREWALL_RULES}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_FIREWALL_RULES not defined, exiting ..." 1>&2
  exit 15
elif [[ -z "${JUMPBOX_FIREWALL_SCRIPT_LINES}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_FIREWALL_SCRIPT_LINES not defined, exiting ..." 1>&2
  exit 16
elif [[ -z "${JUMPBOX_FIREWALL_RULES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_FIREWALL_RULES_FP not defined, exiting ..." 1>&2
  exit 17
elif [[ -z "${JUMPBOX_FIREWALL_SCRIPT_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_FIREWALL_SCRIPT_FP not defined, exiting ..." 1>&2
  exit 18
elif [[ -z "${STACKSCRIPT_BASH_LIBRARY_FN}" ]] ; then
  echo -e "${ERROR_PREFIX} STACKSCRIPT_BASH_LIBRARY_FN not defined, exiting ..." 1>&2
  exit 19
elif [[ -z "${MY_BASH_LIBRARY_FN}" ]] ; then
  echo -e "${ERROR_PREFIX} MY_BASH_LIBRARY_FN not defined, exiting ..." 1>&2
  exit 20
# no: this will be created
# elif [[ ! -r "${JUMPBOX_FIREWALL_RULES_FP}" ]] ; then
#  echo -e "${ERROR_PREFIX} cannot read firewall rules file='${JUMPBOX_FIREWALL_RULES_FP}', exiting ..." 1>&2
#  exit 21
# this will also be created
# elif [[ ! -r "${JUMPBOX_FIREWALL_SCRIPT_FP}" ]] ; then
#   echo -e "${ERROR_PREFIX} cannot read firewall script file='${JUMPBOX_FIREWALL_SCRIPT_FP}', exiting ..." 1>&2
#   exit 22
fi

### ----------------------------------------------------------------------
### functions
### ----------------------------------------------------------------------

STACKSCRIPT_BASH_LIBRARY_FP="${THIS_DIR}/${STACKSCRIPT_BASH_LIBRARY_FN}" # filepath
if   [[ -z "${STACKSCRIPT_BASH_LIBRARY_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} StackScript bash library path=STACKSCRIPT_BASH_LIBRARY_FP is not defined, exiting ..." 1>&2 # stderr
  exit 23
elif [[ ! -r "${STACKSCRIPT_BASH_LIBRARY_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read StackScript bash library='${STACKSCRIPT_BASH_LIBRARY_FP}', exiting ..." 1>&2
  exit 24
else
  source "${STACKSCRIPT_BASH_LIBRARY_FP}"
fi

MY_BASH_LIBRARY_FP="${THIS_DIR}/${MY_BASH_LIBRARY_FN}" # filepath
if   [[ -z "${MY_BASH_LIBRARY_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} library path=MY_BASH_LIBRARY_FP is not defined, exiting ..." 1>&2 # stderr
  exit 25
elif [[ ! -r "${MY_BASH_LIBRARY_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read library='${MY_BASH_LIBRARY_FP}', exiting ..." 1>&2
  exit 26
else
  source "${MY_BASH_LIBRARY_FP}"
fi

### ----------------------------------------------------------------------
### payload
### ----------------------------------------------------------------------

if   [[ -r "${JUMPBOX_FIREWALL_RULES_FP}" ]] ; then
  echo -e "${WARNING_PREFIX} previously-written firewall rules file='${JUMPBOX_FIREWALL_RULES_FP}' found, will be overwritten"
  backup "${JUMPBOX_FIREWALL_RULES_FP}" # from MY_BASH_LIBRARY_FP
elif [[ -r "${JUMPBOX_FIREWALL_SCRIPT_FP}" ]] ; then
  echo -e "${WARNING_PREFIX} previously-written firewall script file='${JUMPBOX_FIREWALL_SCRIPT_FP}' found, will be overwritten"
  backup "${JUMPBOX_FIREWALL_SCRIPT_FP}" # from MY_BASH_LIBRARY_FP
fi

## set hostname. uses StackScript Bash Library::system_set_hostname. TODO: test /etc/hostname

for CMD in \
  "backup /etc/hostname" \
  "system_set_hostname '${JUMPBOX_HOSTNAME}'" \
  "cat /etc/hostname" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done

if [[ -r '/etc/default/dhcpcd' ]] ; then
  SET_HOSTNAME_LINES="$(fgrep -e 'SET_HOSTNAME' /etc/default/dhcpcd)"
  if [[ -n "${SET_HOSTNAME_LINES}" ]] ; then
    echo -e "${WARNING_PREFIX} TODO: comment out line(s) setting token='SET_HOSTNAME' in file='/etc/default/dhcpcd'"
  fi
fi
echo # newline

## set IP. TODO: test IP#s, /etc/hosts

for CMD in \
  "backup /etc/hosts" \
  "echo >> /etc/hosts" \
  "echo -e '# added by ${THIS_FN}' >> /etc/hosts" \
  "echo -e '${JUMPBOX_IPV4_IN_HOSTS_FILE}' >> /etc/hosts" \
  "echo -e '${JUMPBOX_IPV6_IN_HOSTS_FILE}' >> /etc/hosts" \
  "cat /etc/hosts" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done
echo # newline

## set timezone as desired: this replaces `dpkg-reconfigure tzdata`

for CMD in \
  "backup /etc/timezone" \
  "echo '${JUMPBOX_TZ_LOCATION}' > /etc/timezone" \
  "dpkg-reconfigure -f noninteractive tzdata" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done
echo # newline

## add new user, after ensuring package=`sudo` is installed.
## Note `chpasswd` is the batch-mode/non-interactive `passwd`
## TODO: test input strings and paths, (output) results
## TODO: replace with code from StackScript Bash Library::???

for CMD in \
  "aptitude -y install sudo" \
  "adduser ${JUMPBOX_USER_NAME} --disabled-password --gecos ''" \
  "echo '${JUMPBOX_USER_NAME}:${JUMPBOX_USER_PASSWORD}' | chpasswd" \
  "usermod -aG sudo ${JUMPBOX_USER_NAME}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done
echo # newline

if [[ ! -w "${JUMPBOX_USER_HOME}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot write to home dir='${JUMPBOX_USER_HOME}' for new user='${JUMPBOX_USER_NAME}', exiting ..."
else
  # setup user's SSH with this::user_add_pubkey (extends StackScript Bash Library::user_add_pubkey)
  # disable SSH login as user=root with StackScript Bash Library::ssh_disable_root
  for CMD in \
    "user_add_pubkey '${JUMPBOX_USER_NAME}' '${JUMPBOX_USER_PUBLIC_SSHKEY}'" \
    "ls -al ${JUMPBOX_USER_KEYS_FP}" \
    "ls -ald ${JUMPBOX_USER_SSH_DIR}" \
    "ssh_disable_root" \
    "fgrep -nH -e 'PermitRootLogin' /etc/ssh/sshd_config" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done
fi
echo # newline

## setup firewall

echo -e "${MESSAGE_PREFIX} initial iptables set="
for CMD in \
  "iptables -L" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done

#  "cat ${JUMPBOX_FIREWALL_RULES_FP}" \ # superfluous
for CMD in \
  "backup ${JUMPBOX_FIREWALL_RULES_FP}" \
  "backup ${JUMPBOX_FIREWALL_SCRIPT_FP}" \
  "echo -e '${JUMPBOX_FIREWALL_RULES}' > ${JUMPBOX_FIREWALL_RULES_FP}" \
  "iptables-restore < ${JUMPBOX_FIREWALL_RULES_FP}" \
  "iptables -L" \
  "echo -e '${JUMPBOX_FIREWALL_SCRIPT_LINES}' > ${JUMPBOX_FIREWALL_SCRIPT_FP}" \
  "chmod +x ${JUMPBOX_FIREWALL_SCRIPT_FP}" \
  "ls -al ${JUMPBOX_FIREWALL_SCRIPT_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done
echo # newline

## install packages:
## `system_update` from StackScript Bash Library
## `goodstuff` above overrides implementation in StackScript Bash Library 

for CMD in \
  "system_update" \
  "goodstuff" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done
echo # newline

## configure Fail2Ban? no, take defaults. see https://www.linode.com/docs/security/securing-your-server/

## done!
restartServices # StackScript Bash Library::restartServices
echo -e "
${THIS_FN}: complete! Now, return to your client (i.e., logout of this SSH session), and verify that
1. you *cannot* now SSH into your linode as user=root
2. you *can* now SSH into your linode as user='${JUMPBOX_USER_NAME}', with key authentication and without password challenge
3. your desired packages (e.g., for your editor) are installed on your linode
4. your 'sudo iptables -L' output
4.1. resembles listing @ https://www.linode.com/docs/security/securing-your-server/#creating-a-firewall
4.2. ... and your ability to 'sudo' tests that you set your user password correctly.
"

exit 0
