#!/usr/bin/env bash

### Part of project=headful_EPA_jumpbox_config , see https://bitbucket.org/tlroche/headful_epa_jumpbox_config/wiki/Home

### Start (on jumpbox) previously-installed (with install_Firefox_for_F5NAP.sh) downlevel Firefox required for F5NAP (details @ wiki).
### You probably want to start this
### * from a VNC connection (previously started), to interact with its GUI. This script is believed to be compatible with X-forwarding, but that (like VNC) must be setup outside this script.
### * from its own terminal, since it does not run its Firefox with '&'

### Requires:

### * GNU coreutils, e.g., `basename`, `dirname`, `ls`, `mkdir`, `mv`, `popd`, `pushd`, `readlink`
### * `ls`

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project in "the normal way," you could pass the path like:
### > DIR="${PATH_TO_PROJECT}/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" "${DIR}/start_Firefox_for_F5NAP.sh"

### TODO:

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}
THIS_FN='start_Firefox_for_F5NAP.sh' # change if renamed
#echo -e "${THIS_FN}: THIS='${THIS}'"

# TODO? prohibit `source`ing, since that probably won't work
if [[ -z "${THIS}" || "${THIS}" == '-bash' ]] ; then # we're `source`ing
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

## other needed properties must be set here, which must be set by caller
if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### check other properties
### ----------------------------------------------------------------------

if   [[ -z "${JUMPBOX_F5NAP_FIREFOX_PROFILE_NAME}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_F5NAP_FIREFOX_PROFILE_NAME not defined, exiting ..."
  exit 4
elif [[ -z "${JUMPBOX_F5NAP_FIREFOX_PROFILE_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_F5NAP_FIREFOX_PROFILE_DIR not defined, exiting ..."
  exit 5
elif [[ ! -d "${JUMPBOX_F5NAP_FIREFOX_PROFILE_DIR}" ]] ; then
  # make it
  for CMD in \
    "mkdir -p ${JUMPBOX_F5NAP_FIREFOX_PROFILE_DIR}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
    # test {process,return,error} {code,value}
    if [[ "$?" != 0 ]] ; then
      echo -e "${ERROR_PREFIX}: failed '${CMD}', exiting ..."
      exit 6
    fi
  done
elif [[ -z "${F5NAP_F5VPN_SITE}" ]] ; then
  echo -e "${ERROR_PREFIX} F5NAP_F5VPN_SITE not defined, exiting ..."
  exit 7
elif [[ -z "${JUMPBOX_F5NAP_FIREFOX_EXEC_ARGS}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_F5NAP_FIREFOX_EXEC_ARGS not defined, exiting ..."
  exit 8
elif [[ -z "${JUMPBOX_F5NAP_FIREFOX_EXEC_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_F5NAP_FIREFOX_EXEC_FP not defined, exiting ..."
  exit 9
elif [[ ! -x "${JUMPBOX_F5NAP_FIREFOX_EXEC_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot execute F5NAP Firefox @ '${JUMPBOX_F5NAP_FIREFOX_EXEC_FP}', exiting ..."
  exit 10
fi

### ----------------------------------------------------------------------
### run the F5NAP'ed Firefox using specified profile
### ----------------------------------------------------------------------

# note: cannot `eval "whatever &"`
for CMD in \
  "ls -ald ${JUMPBOX_F5NAP_FIREFOX_PROFILE_DIR}" \
  "ls -al ${JUMPBOX_F5NAP_FIREFOX_EXEC_FP}" \
  "${JUMPBOX_F5NAP_FIREFOX_EXEC_FP} ${JUMPBOX_F5NAP_FIREFOX_EXEC_ARGS}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
  # test {process,return,error} {code,value}
  # don't test {process,return,error} {code,value}: they're either trivial or long-running
  if [[ "$?" != 0 ]] ; then
    echo -e "${ERROR_PREFIX}: failed '${CMD}', exiting ..."
    exit 11
  fi
done
echo # newline

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
