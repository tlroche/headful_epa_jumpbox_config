#!/usr/bin/env bash

### Part of project=headful_EPA_jumpbox_config , see https://bitbucket.org/tlroche/headful_epa_jumpbox_config/wiki/Home

### Install downlevel Firefox required for F5NAP required for F5VPN: see wiki.
### Unfortunately this can only be installed
### * from source
### * for 32-bit, which this code assumes is foreign architecture

### Requires:
### * Debian packaging:
### ** version new enough to support new-style multiarch
### ** utilities, e.g., `dpkg`, `aptitude`
### * `wget` and HTTP connection: downloads Firefox code from Mozilla
### * `find`
### * GNU coreutils, e.g., `basename`, `dirname`, `ls`, `mkdir`, `mv`, `popd`, `pushd`, `readlink`

### Requires path to public.properties=PUBLIC_PROPERTIES_FP set in parent environment.
### Presuming you checked out the project in "the normal way," you could pass the path like:
### > DIR="${PATH_TO_PROJECT}/scripts" ; PUBLIC_PROPERTIES_FP="${DIR}/public.properties" "${DIR}/install_Firefox_for_F5NAP.sh"

### TODO:

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## just for logging

THIS="$0"
# `source` -> !useful ${THIS}
THIS_FN='install_Firefox_for_F5NAP.sh'
#echo -e "${THIS_FN}: THIS='${THIS}'"

# TODO? prohibit `source`ing, since that probably won't work
if [[ -z "${THIS}" ||"${THIS}" == '-bash' ]] ; then # we're `source`ing
  # also for manual testing
  THIS_DIR='.' # gotta assume location of other dependencies
else
  THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
  THIS_FN="$(basename ${THIS})"
fi

MESSAGE_PREFIX="${THIS_FN}:"
WARNING_PREFIX="${MESSAGE_PREFIX} WARNING:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### ----------------------------------------------------------------------
### code
### ----------------------------------------------------------------------

## other needed properties must be set here, which must be set by caller
if   [[ -z "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} PUBLIC_PROPERTIES_FP not defined, exiting ..." 1>&2 # stderr
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 2
  fi
elif [[ ! -r "${PUBLIC_PROPERTIES_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read properties file='${PUBLIC_PROPERTIES_FP}', exiting ..." 1>&2
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 3
  fi
else
  source "${PUBLIC_PROPERTIES_FP}"
fi

### ----------------------------------------------------------------------
### check other properties
### ----------------------------------------------------------------------

if   [[ -z "${F5NAP_FIREFOX_SITE}" ]] ; then
  echo -e "${ERROR_PREFIX} F5NAP_FIREFOX_SITE not defined, exiting ..."
  exit 4
elif [[ -z "${F5NAP_FIREFOX_VERSION}" ]] ; then
  echo -e "${ERROR_PREFIX} F5NAP_FIREFOX_VERSION not defined, exiting ..."
  exit 5
elif [[ -z "${F5NAP_FIREFOX_LINUX_VERSION}" ]] ; then
  echo -e "${ERROR_PREFIX} F5NAP_FIREFOX_LINUX_VERSION not defined, exiting ..."
  exit 6
elif [[ -z "${F5NAP_FIREFOX_WITH_VERSION}" ]] ; then
  echo -e "${ERROR_PREFIX} F5NAP_FIREFOX_WITH_VERSION not defined, exiting ..."
  exit 7
elif [[ -z "${F5NAP_FIREFOX_DOWNLOAD_URI}" ]] ; then
  echo -e "${ERROR_PREFIX} F5NAP_FIREFOX_DOWNLOAD_URI not defined, exiting ..."
  exit 8
elif [[ -z "${JUMPBOX_F5NAP_FIREFOX_BUILD_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_F5NAP_FIREFOX_BUILD_DIR not defined, exiting ..."
  exit 9
elif [[ -z "${JUMPBOX_F5NAP_FIREFOX_UNZIP_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_F5NAP_FIREFOX_UNZIP_DIR not defined, exiting ..."
  exit 10
elif [[ -z "${JUMPBOX_F5NAP_FIREFOX_INSTALL_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_F5NAP_FIREFOX_INSTALL_DIR not defined, exiting ..."
  exit 11
elif [[ -d "${JUMPBOX_F5NAP_FIREFOX_INSTALL_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} install dir='${JUMPBOX_F5NAP_FIREFOX_INSTALL_DIR}' exists; delete before proceeding. Exiting ..."
  exit 12
elif [[ -z "${F5NAP_FIREFOX_EXEC_FN}" ]] ; then
  echo -e "${ERROR_PREFIX} F5NAP_FIREFOX_EXEC_FN not defined, exiting ..."
  exit 13
elif [[ -z "${JUMPBOX_F5NAP_FIREFOX_EXEC_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} JUMPBOX_F5NAP_FIREFOX_EXEC_FP not defined, exiting ..."
  exit 14
elif [[ -z "${F5NAP_FIREFOX_PACKAGE_LIST}" ]] ; then
  echo -e "${ERROR_PREFIX} F5NAP_FIREFOX_PACKAGE_LIST not defined, exiting ..."
  exit 15
fi

### ----------------------------------------------------------------------
### install Firefox from source
### ----------------------------------------------------------------------

# TODO: skip if ${JUMPBOX_F5NAP_FIREFOX_INSTALL_DIR}" not empty?
# TODO: skip if [[ -x "${JUMPBOX_F5NAP_FIREFOX_EXEC_FP}" ]] ?
# `tar j` == bzip2
for CMD in \
  "mkdir -p ${JUMPBOX_F5NAP_FIREFOX_BUILD_DIR}" \
  "mkdir -p $(dirname ${JUMPBOX_F5NAP_FIREFOX_INSTALL_DIR})" \
  "pushd ${JUMPBOX_F5NAP_FIREFOX_BUILD_DIR}" \
  "wget -O - ${F5NAP_FIREFOX_DOWNLOAD_URI} | tar xfj -" \
  "popd" \
  "find ${JUMPBOX_F5NAP_FIREFOX_BUILD_DIR} | wc -l" \
  "mv ${JUMPBOX_F5NAP_FIREFOX_UNZIP_DIR} ${JUMPBOX_F5NAP_FIREFOX_INSTALL_DIR}" \
  "du -hs ${JUMPBOX_F5NAP_FIREFOX_INSTALL_DIR}" \
  "ls -alh ${JUMPBOX_F5NAP_FIREFOX_EXEC_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
  # test {process,return,error} {code,value} (TODO: gotta standardize usage in my own code, for `grep`ability)
  if [[ "$?" != 0 ]] ; then
    echo -e "${ERROR_PREFIX}: failed '${CMD}', exiting ..."
    exit 16
  fi
done
echo # newline

### ----------------------------------------------------------------------
### test fresh install: should fail due to lack of 32-bit/foreign dependencies
### ----------------------------------------------------------------------

for CMD in \
  "${JUMPBOX_F5NAP_FIREFOX_EXEC_FP} --version" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
  # so don't fail on {process,return,error} {code,value} here
#  echo -e "${MESSAGE_PREFIX} \$?=='$?'" # debug: note this will cause following test of $? to succeed!
done
# echo # newline? no, because, again, this will cause following test of $? to succeed!

### ----------------------------------------------------------------------
### fix fresh install
### ----------------------------------------------------------------------

if [[ "$?" == 0 ]] ; then
  echo # newline
  echo -e "${MESSAGE_PREFIX}: F5NAPable Firefox executable='${JUMPBOX_F5NAP_FIREFOX_EXEC_FP}' works, exiting ..."
  # not for `source`ing
  if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
    exit 0
  fi
fi
# else

## install needed packages
## TODO: skip update if `dpkg --print-foreign-architectures` includes 'i386'
for CMD in \
  'sudo dpkg --add-architecture i386' \
  'sudo aptitude update' \
  "sudo aptitude install -y ${F5NAP_FIREFOX_PACKAGE_LIST}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
  if [[ "$?" != 0 ]] ; then
    echo -e "${ERROR_PREFIX}: failed '${CMD}', exiting ..."
    exit 17
  fi
done
echo # newline

### ----------------------------------------------------------------------
### test fresh install: should succeed
### not starting full-GUI firefox since we want to use VNC to see it
### ----------------------------------------------------------------------

for CMD in \
  "${JUMPBOX_F5NAP_FIREFOX_EXEC_FP} --version" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
  if [[ "$?" != 0 ]] ; then
    echo -e "${ERROR_PREFIX}: failed '${CMD}', exiting ..."
    exit 18
  fi
done
echo # newline

### ----------------------------------------------------------------------
### teardown
### ----------------------------------------------------------------------

# not for `source`ing
if [[ -n "${THIS}" && "${THIS}" != '-bash' ]] ; then
  exit 0
fi
